let gameContainer = document.querySelector("#game_container"); 
let character = document.querySelector("#mario"); 
let obstacle = document.querySelector("#bowser");
let floor = document.querySelector("#floor");
let luckyblock = document.querySelector("#luckyblock")
let score = document.querySelector("#score"); 
let gameover = document.querySelector("#gameover");
let controls = document.querySelector("#controls")

// Variable score

let interval = null;
let playerScore = 0;

// Fonction score

let scoreCounter = ()=> {
    playerScore++;
    score.innerHTML = `Score ${playerScore}`;
}

// Start Game

window.addEventListener("keydown" , (start)=> {
    // console.log(start);
    if(start.code == "Space") {
        gameover.style.display = "none";
        controls.style.display = "none";
        obstacle.classList.add("bowseranim");
        floor.firstElementChild.style.animation = "animFloor 1.5s linear infinite";
        luckyblock.firstElementChild.style.animation = "animLuckyblock 10s linear infinite";

        let playerScore = 0;
        interval = setInterval(scoreCounter,300);
    }
});

// Saut

window.addEventListener("keydown", (x)=>{
    // console.log(x);

    if(x.key == "ArrowUp")
        if(character.classList != "marioanim") {
            character.classList.add("marioanim");


            setTimeout(()=>{
                character.classList.remove("marioanim");
            },500);
        }
});

// Game Over

let result = setInterval(()=>{
    let characterBottom = parseInt(getComputedStyle(character).getPropertyValue("bottom"));
    // console.log("characterBottom" + charaterBottom);

    let obstacleLeft = parseInt(getComputedStyle(obstacle).getPropertyValue("left"));
    // console.log("obstacleLeft" + obstacleLeft);

    if(characterBottom <= 90 && obstacleLeft >= 20 && obstacleLeft <= 145) {
    //  console.log("Game Over")

        gameover.style.display = "block";
        controls.style.display = "block";
        obstacle.classList.remove("bowseranim");
        floor.firstElementChild.style.animation = "none";
        luckyblock.firstElementChild.style.animation = "none";
        clearInterval(interval);
        playerScore = 0;
    }
},10);
