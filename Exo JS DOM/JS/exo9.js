// Exo 9 JS DOM

const sizetab = 10

function generateTableColor(){

    var tabcolor = document.getElementById("tabColor");

    // Création tableau

    var table = document.createElement("table");
    table.classList.add("bordure")
    var tableBody =  document.createElement("tbody")

    // Création cellule
    for (var i = 0; i < sizetab; i++) {

        // Création des rows
        var row = document.createElement("tr");

        for (var e = 0; e < sizetab; e++) {
            var cellule = document.createElement("td")
            if (i == 0 || i == sizetab-1 || e == 0 ||e == sizetab-1) {
                var celluleText = document.createTextNode("1")
                cellule.classList.add("green")
            } else {
                var celluleText = document.createTextNode("0")
                cellule.classList.add("red")
            }

            cellule.appendChild(celluleText)
            row.appendChild(cellule)
        }

        tableBody.appendChild(row)
    }

    table.appendChild(tableBody)
    tabcolor.appendChild(table)
}
