    // EXO JS DOM 7

    const sizetab = 10

    function generateTable(){

        var tab = document.getElementById("tableau");

        // Création tableau

        var table = document.createElement("table");
        table.classList.add("bordure")
        var tableBody =  document.createElement("tbody");

        // Création cellule
        for (var i = 0; i < sizetab; i++) {

            // Création des rows
            var row = document.createElement("tr");


            for (var e = 0; e < sizetab; e++) {

                var cellule = document.createElement("td")
                var celluleText = document.createTextNode("0")
                cellule.appendChild(celluleText)
                row.appendChild(cellule)
            }

            tableBody.appendChild(row)
        }

        table.appendChild(tableBody)
        tab.appendChild(table)
    }
